class gameBasic {
  constructor() {
    this.initialize('all');
  }

  initialize(type = 'all', data = null) {
    if (type == 'all' || type == 'game') {
      console.log(type);
      this.game = {
        status: 0, // 0: waiting, 1: start, 2: playing, 3: finish
        rule: {
          question_counts: -1 // 題數, -1: 無限制
        },
        question: null,
        statistics: {
          now: 0,             // 目前題數
          firstCorrect: 0,   // 首次變答對的次數
        },
        isFirstToAnswer: true,
        startTime: null,
        totalTime: null,
      };
    }

    if (type == 'all' || type == 'controller') {
      console.log(type);
      this.controller = {
        audio: true, // true: open, false: close
        raceMode: false,
      };
    }
  }

  startGame() {
    this.setQuestionAndAnswer();
    this.setOptional(this.game.question);
    this.game.status = 1;
    this.game.isFirstToAnswer = true;
    this.game.statistics.now += 1;

    // 第一局開始，紀錄開始時間
    if (this.game.statistics.now == 1) {
      this.game.startTime = Date.now();
    }
  }

  playGame() {
    this.game.status = 2;
  }

  finishGame() {
    this.game.status = 3;
    if (this.game.isFirstToAnswer === true) {
      this.game.statistics.firstCorrect += 1;
    }

    // 競賽最後一局紀錄時間
    if (this.controller.raceMode === true && this.game.rule.question_counts < this.game.statistics.now) {
      this.game.totalTime = Date.now();
      return false;
    }

    return true;
  }

  getRandomNumber() {
    return Math.floor(Math.random() * 10) + 1;
  }

  setQuestionAndAnswer() {
    const question = {};
    question.val_left = this.getRandomNumber();
    question.val_right = this.getRandomNumber();
    question.result = question.val_left + question.val_right;
    this.game.question = question;
  }

  setOptional() {
    const question = this.game.question;

    // 範圍 2 ~ 20
    const possible_answer = [];
    for (let i = 2; i <= 20; i++) {
      possible_answer.push(i);
    }

    // 先拔掉正確答案
    const filteredArray = possible_answer.filter(item => item !== question.result);

    const selectedValues = [question.result];

    // 隨機拔出三個選項
    while (selectedValues.length < 4) {
      const randomIndex = Math.floor(Math.random() * filteredArray.length);
      selectedValues.push(filteredArray[randomIndex]);

      // 移除選中的值
      filteredArray.splice(randomIndex, 1);
    }

    this.game.question.optional = selectedValues.sort(() => Math.random() - 0.5);
  }
  
}

$(document).ready(function() {
  // ... (保留你的文件就緒時的程式碼)

  const gameInstance = new gameBasic();

  // setTimeout(initialize, 2000);
  initialize();

  function initialize() {
    restartGame();
  }

  function restartGame() {
    gameInstance.startGame();
    refresh_screen(gameInstance.game);
    gameInstance.playGame();
  }

  function refresh_screen(game) {
    // 還原狀態
    $("#animation_box").removeClass("circle");
    $("#opt_1, #opt_2, #opt_3, #opt_4").addClass('normal').removeClass('failure').removeClass('correct');

    // 賦予值
    $("#left_side").text(game.question.val_left);
    $("#right_side").text(game.question.val_right);
    $("#opt_1").val(game.question.optional[0]).text(game.question.optional[0]);
    $("#opt_2").val(game.question.optional[1]).text(game.question.optional[1]);
    $("#opt_3").val(game.question.optional[2]).text(game.question.optional[2]);
    $("#opt_4").val(game.question.optional[3]).text(game.question.optional[3]);

    let game_mode = (gameInstance.controller.raceMode === true) ? "【競賽模式】" : "【練習模式】";
    let statistics = game_mode + '目前題數：' + game.statistics.now + '／首次便答對題數：' + game.statistics.firstCorrect;
    $("#bottom_block").text(statistics);

    if (gameInstance.controller.raceMode === true) {
      if (game.statistics.now === 1) {
        $('#switch_race-mode .switch_race-mode-on span').text('(' + game.rule.question_counts + ')');
      } else {
        $('#switch_race-mode .switch_race-mode-on span').text('(' + (game.rule.question_counts - game.statistics.now + 1) + ')');
      }
    }
  }

  $("#opt_1, #opt_2, #opt_3, #opt_4").click(function () {
    if (gameInstance.game.status != 2) return false;

    var select_value = $(this).val();
    var is_correct = (select_value == gameInstance.game.question.result);
    if (is_correct) {
      var hasNext = gameInstance.finishGame();
      if (gameInstance.controller.audio) {
        var audio = new Audio('assets/audio/correct.mp3');
        audio.currentTime = 0.5;
        audio.play();
      }

      // $("#animation_box").removeClass("X-animation");
      $("#animation_box").addClass("circle");
      $(this).addClass('correct');
      if (hasNext) {
        setTimeout(function() {
          restartGame();
        }, 1000);
      } else {
        // 沒有下一局，統計資訊做呈現
        setTimeout(function() {
          $("#animation_box").removeClass("circle");
          $("#opt_1, #opt_2, #opt_3, #opt_4").addClass('normal').removeClass('failure').removeClass('correct');

          timeDiff = ((gameInstance.game.totalTime - gameInstance.game.startTime) / 1000).toFixed(2);
          $("#bottom_block").append('結束，用時 <font style="#00FF:;">' + timeDiff + '<font> 秒')
          $("#popup .popup-content p").html('用時 <font style="#00FF:;">' + timeDiff + '<font> 秒');
          $("#popup").show();
        }, 1000);

      }
    } else {
      gameInstance.game.isFirstToAnswer = false;
      if (gameInstance.controller.audio) {
        var audio = new Audio('assets/audio/error_funny.mp3');
        audio.currentTime = 0.5;
        audio.play();            
      }

      $(this).removeClass('normal').addClass('failure');
      // $("#animation_box").addClass("X-animation");
      // setTimeout(function() {
      //   $("#animation_box").removeClass("X-animation");
      // }, 1000);
    }
  });

  $("#closePopupBtn").click(function () {
    $("#popup").hide();
  });

  $("#switch_audio").click(function () {
    gameInstance.controller.audio = !$('#switch_audio .switch_audio-on').is(':visible');
    $('#switch_audio .switch_audio-on').toggle();;
    $('#switch_audio .switch_audio-off').toggle();
  });
  $("#switch_race-mode").click(function () {
    gameInstance.controller.raceMode = !$('#switch_race-mode .switch_race-mode-on').is(':visible');
    $('#switch_race-mode .switch_race-mode-on').toggle();
    $('#switch_race-mode .switch_race-mode-off').toggle();

    if (gameInstance.controller.raceMode === true) {
      gameInstance.game.rule.question_counts = 10;
    } else {
      gameInstance.game.rule.question_counts = -1;
    }

    gameInstance.game.statistics.now = 0;
    gameInstance.game.statistics.firstCorrect = 0;
    restartGame();
  });
});